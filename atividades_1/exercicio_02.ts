/*Faça um programa que receba três notas, calcule e mostre a
média aritmética entre elas.*/

//bloco exercicio_2
namespace exercicio_2
{

    let nota1, nota2, nota3, media: number;  //dados

    nota1 = 10;
    nota2 = 7;
    nota3 = 9;

    media = (nota1 + nota2 + nota3) / 2;  //processo

    console.log(`A média aritimética entre ${nota1}, ${nota2} e ${nota3} é: \n ${media}.`);  //saída

}