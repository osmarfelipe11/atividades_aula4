/*Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R^2 */

namespace exercicio_10.ts
{
    let area, raio: number;

    raio = 6;

    area = Math.PI * raio ** 2;

    console.log(`A area de um círculo com raio ${raio} é: ${area}.`);
}