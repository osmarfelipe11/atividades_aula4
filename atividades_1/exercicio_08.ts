/*Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.*/

namespace exercicio_08
{
    let deposito, taxa, rendimento, valor_total: number;

    deposito = 100;
    taxa = 4;

    rendimento = deposito * taxa / 100;
    valor_total = deposito + rendimento;

    console.log(`Rendimento: ${rendimento} \n Valor total depois do rendimento: ${valor_total}.`);
}