/*Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.*/

namespace exercicio_09
{
    let base, altura, area: number;

    base = 7;
    altura = 9;

    area = (base * altura) / 2;

    console.log(`A area de um triângulo com base ${base} e altura ${altura} é: ${area}.`);
}