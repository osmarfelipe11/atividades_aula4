/*Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.*/

namespace exercicio_06
{
    let salario_base, desconto, bonus, salario: number;

    salario_base = 1000;

    desconto = salario_base * 7 / 100;
    bonus = salario_base * 5 / 10;
    salario = salario_base + bonus - desconto;
    
    console.log(`O salario final com bônus e imposto é ${salario}`)
}