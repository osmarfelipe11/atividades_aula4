/*Faça um programa que receba o salário de um funcionário,
calcule e mostre o novo salário, sabendo-se que este sofreu
um aumento de 25%.*/

namespace exercicio_04 
{
    let salario1, salario2: number;

    salario1 = 1000;
    salario2 = salario1 + (salario1 * 25 / 100);

    console.log(`O novo salário, com aumento de 25% é: ${salario2}`);
}