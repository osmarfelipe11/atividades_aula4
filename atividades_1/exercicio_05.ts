/*Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário.*/

namespace exercicio_05
{
    let salario1, aumento, v_aumento, salario2: number;

    salario1 = 1000;
    aumento = 30;
    v_aumento = salario1 * aumento / 100;
    salario2 = v_aumento + salario1;

    console.log(`O valor do aumento é: ${v_aumento} \n O novo salário é R$ ${salario2}.`);
}