/*Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga
imposto de 10% sobre o salário-base.*/

namespace exercicio_07
{
    let salario_base, desconto, salario: number;

    salario_base = 1000;

    desconto = salario_base * 10 / 100;
    salario = salario_base + 50 - desconto;

    console.log(`O salário do funcionário com gratificação de R$50,00 e 10% de imposto é: ${salario}`);
}