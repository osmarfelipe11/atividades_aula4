/*Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.*/

namespace exercicio_03
{
    let nota1, nota2, nota3, peso1, peso2, peso3, media_p: number;

    nota1 = 8;
    nota2 = 5;
    nota3 = 6;
    peso1 = 3;
    peso2 = 1;
    peso3 = 4;

    media_p = (nota1 * peso1 + nota2 * peso2 + nota3 + peso3) / (peso1 + peso2 + peso3);

   console.log(`A média ponderada entre ${nota1}, ${nota2} e ${nota3}, com peso ${peso1}, ${peso2} e ${peso3} respectivamente, é: \n ${media_p}.`);
}